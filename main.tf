resource "kubernetes_secret" "repository" {
  metadata {
    name      = var.app_name
    namespace = var.argocd_namespace
    labels = {
      "argocd.argoproj.io/secret-type" = "repository"
    }
  }

  data = {
    project = var.argocd_project
    type    = "git"
    url     = var.app_repo_url
  }

  type = "Opaque"
}

resource "kubernetes_manifest" "argocd_application" {
  manifest = yamldecode(
    templatefile("${path.module}/argocd-application.yaml", {
      name             = var.app_name
      argocd_namespace = var.argocd_namespace
      argocd_project   = var.argocd_project
      repo             = var.app_repo_url
      namespace        = var.app_name
      environment      = var.project_environment
      project          = var.project_name
      crt              = var.app_crt
      key              = var.app_crt_key
    })
  )
  depends_on = [kubernetes_secret.repository]
}

resource "cloudflare_record" "dns" {
  zone_id = data.cloudflare_zone.main.zone_id
  name    = module.name.pretty
  value   = var.ingress_public_ip
  type    = "A"
  ttl     = "300"
  proxied = var.cf_proxy_enabled
}