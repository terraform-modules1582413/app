<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_cloudflare"></a> [cloudflare](#requirement\_cloudflare) | ~> 4.0.0 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | ~> 2.18.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_cloudflare"></a> [cloudflare](#provider\_cloudflare) | ~> 4.0.0 |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | ~> 2.18.1 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_name"></a> [name](#module\_name) | git::https://gitlab.com/terraform-modules1582413/name.git | n/a |

## Resources

| Name | Type |
|------|------|
| [cloudflare_record.dns](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/record) | resource |
| [kubernetes_manifest.argocd_application](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/manifest) | resource |
| [kubernetes_secret.repository](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret) | resource |
| [cloudflare_zone.main](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/data-sources/zone) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_app_crt"></a> [app\_crt](#input\_app\_crt) | n/a | `string` | n/a | yes |
| <a name="input_app_crt_key"></a> [app\_crt\_key](#input\_app\_crt\_key) | n/a | `string` | n/a | yes |
| <a name="input_app_name"></a> [app\_name](#input\_app\_name) | n/a | `string` | n/a | yes |
| <a name="input_app_repo_url"></a> [app\_repo\_url](#input\_app\_repo\_url) | n/a | `string` | n/a | yes |
| <a name="input_argocd_namespace"></a> [argocd\_namespace](#input\_argocd\_namespace) | n/a | `string` | `"argocd"` | no |
| <a name="input_argocd_project"></a> [argocd\_project](#input\_argocd\_project) | n/a | `string` | n/a | yes |
| <a name="input_cf_base_domain"></a> [cf\_base\_domain](#input\_cf\_base\_domain) | n/a | `string` | n/a | yes |
| <a name="input_cf_proxy_enabled"></a> [cf\_proxy\_enabled](#input\_cf\_proxy\_enabled) | n/a | `bool` | `false` | no |
| <a name="input_ingress_public_ip"></a> [ingress\_public\_ip](#input\_ingress\_public\_ip) | n/a | `string` | n/a | yes |
| <a name="input_project_environment"></a> [project\_environment](#input\_project\_environment) | n/a | `string` | n/a | yes |
| <a name="input_project_name"></a> [project\_name](#input\_project\_name) | n/a | `string` | n/a | yes |
| <a name="input_project_prefix"></a> [project\_prefix](#input\_project\_prefix) | n/a | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->