variable "project_prefix" {
  type = string
}
variable "project_name" {
  type = string
}
variable "project_environment" {
  type = string
}

variable "argocd_project" {
  type = string
}
variable "argocd_namespace" {
  type    = string
  default = "argocd"
}

variable "cf_base_domain" {
  type = string
}
variable "cf_proxy_enabled" {
  type    = bool
  default = false
}

variable "ingress_public_ip" {
  type = string
}

variable "app_name" {
  type = string
}
variable "app_crt_key" {
  type      = string
  sensitive = true
}
variable "app_crt" {
  type      = string
  sensitive = true
}
variable "app_repo_url" {
  type = string
}
